from django.contrib import admin
from django.core.exceptions import PermissionDenied
from django.http import HttpResponse
from django.urls import path

from main import views
from main.views import handler404View, handler403View, handler400View, handler500View
from django.contrib.auth import views as auth_views

#  --- Custom exception-handlers --- #
handler500 = handler500View
handler404 = handler404View
handler403 = handler403View
handler400 = handler400View

urlpatterns = [
    # path('admin/', admin.site.urls),
    path('', views.getHomePage),
    path('vanilla/', views.getVanillaServerPage),
    path('modded/', views.getModdedServerPage),
    path('admin/', views.getAdminPanel),
    path('register/', views.getRegistrationPage),
    path('login/', views.getLoginPage),
    path('logout/', views.logoutView),
    # -- not for users -- #
    path('remove_order/<int:id>', views.removeOrder),
    path('accept_order/<int:id>', views.acceptOrder),
]
