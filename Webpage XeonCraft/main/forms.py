from django import forms


class RegForm(forms.Form):
    userName = forms.CharField(required=True)
    userVK = forms.CharField(required=True)
    userPassword = forms.PasswordInput()


class AuthForm(forms.Form):
    # Имя игрока на сервере
    userName = forms.CharField(required=True)
    # Пароль игрока
    userPassword = forms.PasswordInput()
