import socket
import sqlite3

import django
import psycopg2
from django.contrib.auth.models import User
from django.db import connection, connections
from django.shortcuts import render, redirect

from django.contrib.auth import authenticate, login, logout

from main.forms import AuthForm, RegForm
from main.models import userOrder, IPList, minecraft_server_users

from mcstatus import MinecraftServer

from django.utils import timezone


def getHomePage(request):
    global online_vanilla, max_online_vanilla, version_vanilla, max_online_modded, version_modded, online_modded
    server_vanilla = MinecraftServer.lookup("95.214.9.63:25564")
    server_modded = MinecraftServer.lookup("95.214.9.63:25565")

    status_vanilla = True
    try:
        server_vanilla.status(3)
        online_vanilla = server_vanilla.status().players.online
        max_online_vanilla = server_vanilla.status().players.max
        version_vanilla = server_vanilla.status().version.name
    except:
        status_vanilla = False
        online_vanilla = 0
        max_online_vanilla = 0
        version_vanilla = "Unknown"

    status_modded = True
    try:
        server_modded.status(3)
        online_modded = server_modded.status().players.online
        max_online_modded = server_modded.status().players.max
        version_modded = server_modded.status().version.name
    except:
        status_modded = False
        online_modded = 0
        max_online_modded = 0
        version_modded = "Unknown"

    context = {
        'title': 'XeonCraft | Главная страница',
        # --- vanilla server --- #
        'online_vanilla': online_vanilla,
        'max_online_vanilla': max_online_vanilla,
        'version_vanilla': version_vanilla,
        'status_vanilla': status_vanilla,
        # --- modded server --- #
        'online_modded': online_modded,
        'max_online_modded': max_online_modded,
        'version_modded': version_modded,
        'status_modded': status_modded,
    }

    return render(request, 'index.html', context)


def getRegistrationPage(request):
    context = {
        'title': 'XeonCraft | Подача заявок',
    }

    if request.method == "POST":
        form = RegForm(request.POST)
        ctx = {}

        if form.is_valid():
            name = form.data['userName']
            password = form.data['userPassword']
            vk = form.data['userVK']

            canRegister = True
            try:
                nice_var_bro = IPList.objects.get(userIP=get_client_ip(request=request))
                canRegister = False
            except:
                pass

            if "https://vk.com/" not in vk:
                canRegister = False

            if len(name) > 12 or len(name) < 2:
                canRegister = False

            if len(password) > 16 or len(password) < 8:
                canRegister = False

            # cursor = connections['default'].cursor()
            # cursor.execute('SELECT * FROM auth_user')
            # another_user: str = cursor.fetchone()[4]

            # if name == another_user:
            #     foundDuplicatedField = True

            try:
                user = minecraft_server_users.objects.get(user=name)
                canRegister = False
                ctx = {'reg_error': 'Данный пользователь уже зарегистрирован на сервере! Попробуйте выбрать другой ник.'}
            except:
                pass

            if canRegister:
                order = userOrder(user=name, password=password, userIP=get_client_ip(request=request), userVK=vk)
                order.save()
                # new_ip = IPList(userIP=get_client_ip(request=request))
                # new_ip.save()
            return redirect('/register')
    else:
        return render(request, 'registration.html', context)


def getVanillaServerPage(request):
    context = {
        'title': 'XeonCraft | Сервер Vanilla',
        'players': getPlayersFromServer("95.214.9.63:25564"),
    }

    return render(request, 'servers/vanilla.html', context)


def getModdedServerPage(request):
    context = {
        'title': 'XeonCraft | Сервер DDSH Modded',
        'players': getPlayersFromServer("95.214.9.63:25565"),
    }

    return render(request, 'servers/modded.html', context)


def getAdminPanel(request):
    if not request.user.is_superuser:
        return redirect('/login')
    # cursor = connections['users'].cursor()
    # cursor.execute('SELECT * FROM auth_user')
    # print(cursor.fetchone()[4])

    context = {
        'title': 'XeonCraft | Админ панель',
        'orders': userOrder.objects.using('default').all(),
    }

    return render(request, 'dashboard.html', context)


def removeOrder(request, id):
    if not request.user.is_superuser:
        return redirect('/')
    order = userOrder.objects.get(pk=id)
    order.delete()
    return redirect('/admin')


def acceptOrder(request, id):
    if not request.user.is_superuser:
        return redirect('/')
    order = userOrder.objects.get(pk=id)

    user = minecraft_server_users(user=order.user, date_joined=django.utils.timezone.now())
    user.save()

    for item in userOrder.objects.using('default').all():
        if item.user == order.user:
            userOrder.objects.get(pk=item.id).delete()

    cursor = connections['users'].cursor()
    cursor.execute(
        "INSERT INTO users (login, password) VALUES ('{0}', MD5('{1}'))".format(order.user, order.password)
    )

    return redirect('/admin')


def getLoginPage(request):
    if request.method == "POST":
        form = AuthForm(request.POST)

        if form.is_valid():
            name = form.data['userName']
            password = form.data['userPassword']

            user = authenticate(request, username=name, password=password)
            login(request, user)
            return redirect('/')
        else:
            return redirect('/login')
    else:
        return render(request, 'login.html')


def logoutView(request):
    logout(request)
    return redirect('/')


# --- CUSTOM FUNCTIONS SECTION --- #
# WARNING!!! NOT FOR WEBSITE
# WARNING!!! NOT FOR USING IN PAGE REQUESTS AND ANOTHER SECTIONS
def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def get_open_ports(ip: str) -> list:
    ports: list = []
    for i in range(65535):
        if socket.socket(socket.AF_INET, socket.SOCK_STREAM).connect_ex((ip, i)) == 0:
            ports.append(i)
    return ports


def getPlayersFromServer(ip: str) -> list:
    server = MinecraftServer.lookup(ip)

    players: list = []
    if server.status().players.sample is not None:
        for item in server.status().players.sample:
            players.append(item.name)
    else:
        players.append("Никого нет =(")
    players.sort()
    return players


def handler500View(request, *args, **argv):
    return render(request, 'exceptions/500.html', status=500)


def handler404View(request, *args, **argv):
    return render(request, 'exceptions/404.html', status=404)


def handler403View(request, *args, **argv):
    return render(request, 'exceptions/403.html', status=403)


def handler400View(request, *args, **argv):
    return render(request, 'exceptions/400.html', status=400)
