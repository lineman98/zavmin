import django

from django.db import models
from django.utils import timezone


# Заявка пользователя для регистрации на сайте
class userOrder(models.Model):
    user = models.CharField(max_length=12, null=True, default="")
    password = models.CharField(max_length=16, null=True, default="")
    userIP = models.CharField(max_length=100, null=True, default="unknown")
    userVK = models.CharField(max_length=200, null=True, default="unknown")


# Список айпи которые уже подали заявку на регистрацию
class IPList(models.Model):
    userIP = models.CharField(max_length=50, null=True, default="unknown")


# Уже зарегистрированные пользователи
class minecraft_server_users(models.Model):
    user = models.CharField(max_length=12, null=True, default="")
    date_joined = models.DateTimeField(default=django.utils.timezone.now())
